# -*- coding: utf-8 -*-

# !/usr/bin/python

# Note: requires the tqdm package (pip install tqdm)

# Note to Kagglers: This script will not run directly in Kaggle kernels. You
# need to download it and run it on your local machine.

# Downloads images from the Google Landmarks dataset using multiple threads.
# Images that already exist will not be downloaded again, so the script can
# resume a partially completed download. All images will be saved in the JPG
# format with 90% compression quality.

# Thanks to @maxwell: https://www.kaggle.com/maxwell110/python3-version-image-downloader

import sys, os, multiprocessing, csv
from urllib import request, error
from PIL import Image
from io import BytesIO
import tqdm
import pandas as pd
import re


def parse_data(data_file):
    csvfile = open(data_file, 'r')
    csvreader = csv.reader(csvfile)
    key_url_list = [line[:2] for line in csvreader]
    return key_url_list[1:]  # Chop off header


def remove_already_downloaded(queue, out_dir):
    existing_files = [re.sub('\.jpg$', '', f) for f in os.listdir(out_dir) if os.path.isfile(os.path.join(out_dir, f))]
    short_queue = pd.DataFrame(queue, columns=['name', 'url'])
    short_queue = short_queue[~short_queue.name.isin(existing_files)]
    if len(existing_files) > 0:
        print(str(len(existing_files)) + ' out of ' + str(len(queue)) +
              ' files already downloaded.\nThey will be removed from download queue')
        print('Total files to download: ' + str(len(short_queue)) + '\n')
    queue = short_queue.values.tolist()
    return queue


def download_image(key_url):
    out_dir = sys.argv[2]
    (key, url) = key_url
    filename = os.path.join(out_dir, '{}.jpg'.format(key))

    if os.path.exists(filename):
        print('Image {} already exists. Skipping download.'.format(filename))
        return 0

    try:
        response = request.urlopen(url)
        image_data = response.read()
    except:
        print('Warning: Could not download image {} from {}'.format(key, url))
        return 1

    try:
        with open(filename, 'wb') as output:
            output.write(image_data)
    except:
        print('Warning: Could not download image {} from {}'.format(key, url))
        return 1
    # commented below code
    #try:
    #    pil_image = Image.open(BytesIO(image_data))
    #except:
    #    print('Warning: Failed to parse image {}'.format(key))
    #    return 1

    #try:
    #    pil_image_rgb = pil_image.convert('RGB')
    #except:
    #    print('Warning: Failed to convert image {} to RGB'.format(key))
    #    return 1

    #try:
    #    pil_image_rgb.save(filename, format='JPEG', quality=90)
    #except:
    #    print('Warning: Failed to save image {}'.format(filename))
    #    return 1

    return 0


def loader():
    if len(sys.argv) != 4:
        print('Syntax: {} <data_file.csv> <output_dir/> <num workers>'.format(sys.argv[0]))
        sys.exit(0)
    (data_file, out_dir, nproc) = sys.argv[1:]
    nproc = int(nproc)

    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    key_url_list = parse_data(data_file)
    key_url_list = remove_already_downloaded(key_url_list, out_dir)
    pool = multiprocessing.Pool(processes=nproc)  # Num of CPUs
    failures = sum(tqdm.tqdm(pool.imap_unordered(download_image, key_url_list), total=len(key_url_list)))
    print('Total number of download failures:', failures)
    pool.close()
    pool.terminate()


# arg1 : data_file.csv
# arg2 : output_dir
if __name__ == '__main__':
    loader()