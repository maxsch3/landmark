# -*- coding: utf-8 -*-

# !/usr/bin/python

# Note: requires the tqdm package (pip install tqdm)

# Note to Kagglers: This script will not run directly in Kaggle kernels. You
# need to download it and run it on your local machine.

# Resizes images in multiple threads
# All images will be saved in the JPG format with 90% compression quality.

# Thanks to @maxwell: https://www.kaggle.com/maxwell110/python3-version-image-downloader

import sys, os, multiprocessing, csv
from PIL import Image
import tqdm
import pandas as pd
import re


def list_images(inp_dir):
    files = [[re.sub('\.jpg$', '', f), os.path.join(inp_dir, f)] for f in os.listdir(inp_dir) if os.path.isfile(os.path.join(inp_dir, f))]
    return files


def image_info(img_path):
    out_dir = sys.argv[2]
    new_size = int(sys.argv[3])
    (img, path) = img_path
    filename = os.path.join(out_dir, '{}.jpg'.format(img))

    try:
        pil_image = Image.open(path)
    except:
        print('Warning: Failed to open image {}'.format(img))
        return [img, '', 0, 0, 0]

    w, h = pil_image.size
    file_size = os.path.getsize(path)

    return [img, pil_image.mode, h, w, file_size]


def loader():
    if len(sys.argv) != 4:
        print('Syntax: {} <input dir> <output_file> <nproc>'.format(sys.argv[0]))
        sys.exit(0)
    (inp_dir, out_file, nproc) = sys.argv[1:]
    nproc = int(nproc)

    file_list = list_images(inp_dir)
    pool = multiprocessing.Pool(processes=nproc)  # Num of CPUs
    failures = tqdm.tqdm(pool.imap_unordered(image_info, file_list), total=len(file_list))
    lst = [l for l in failures]
    #print('Total number of failures:', failures)
    pool.close()
    pool.terminate()
    df = pd.DataFrame(lst, columns=['id', 'color_mode', 'height', 'width', 'file_size'])
    df.to_csv(out_file)
    return 0


# arg1 : data_file.csv
# arg2 : output_dir
if __name__ == '__main__':
    loader()