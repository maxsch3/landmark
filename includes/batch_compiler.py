import numpy as np
import pandas as pd
from dask.delayed import delayed
#import os
#from keras.preprocessing.image import img_to_array
#from PIL import Image


class BaseBatchMaker(object):
    def __init__(self, ids, classes, shuffle=True, y_type='classes'):
        if classes is None:
            self.df = pd.DataFrame({'id': ids})
            self.return_y = False
        else:
            self.df = pd.DataFrame({'id': ids, 'class': classes})
            self.return_y = True
        self.df.reset_index(drop=True, inplace=True)
        self.shuffle = shuffle
        assert y_type in ['classes', 'transform']
        self.y_type = y_type

    def epoch_init(self):
        if self.shuffle:
            self.df = self.df.sample(frac=1).reset_index(drop=True)
        return

    def get_indices(self):
        return list(self.df.index)

    @staticmethod
    def reshape(batch_data):
        x, y = zip(*batch_data)
        return np.stack(x), np.stack(y)

    def make_batch(self, ids, loader):
        # X, y = zip(*[loader.load_item(self.df['id'].iloc[i]) for i in ids])
        xy = [delayed(loader.load_item)(self.df['id'].iloc[i]) for i in ids]
        xy = delayed(self.reshape)(xy)
        x, y = xy.compute()
        if self.return_y:
            y = self.make_y(ids, y)
            return x, y
        else:
            return x

    def make_y(self, ids, y):
        if self.y_type == 'classes':
            y = self.df['class'].loc[ids].as_matrix()
        return y


class PPNRandomBatch(object):
    """This class is used for generating batches of 3 images I1, I2, I3 as X
    two of these images are of the same class and one image from different class
    """
    def __init__(self, ids, classes, neg_only=None,
                 y=None, y_type='0', ppn_shuffle=False,
                 r_ratio=0.0, n_ratio=0.0, epoch_size=0, replace_ref=False, replace_neg=False):
        assert y_type in ['id', 'classes', '2dist', '3dist', 'y', '0']
        assert epoch_size >= 0
        assert type(epoch_size) is int
        assert len(ids) == len(classes)
        if neg_only is None:
            neg_only = [False]*len(ids)
        assert len(neg_only) == len(ids)
        self.df = pd.DataFrame({'id': ids, 'class': classes, 'neg_only': neg_only})
        if y is not None and y_type == 'y':
            self.df['y'] = y
        self.df.reset_index(drop=True, inplace=True)
        self.r_ratio = r_ratio
        self.n_ratio = n_ratio
        self.get_weighs()
        self.plan = None
        self.y_type = y_type
        self.ppn_shuffle = ppn_shuffle
        self.epoch_size = epoch_size
        self.replace_ref = replace_ref
        self.replace_neg = replace_neg
        if self.epoch_size == 0:
            self.epoch_size = sum(~self.df['neg_only'])

    def get_weighs(self):
        """This function adds new weight column to self.df data frame to use later in sampling. The weights
        do not sum up to 1 but probabilities can easily be calculated by normalizing them.

        The weights are always the same within a category, so instances of same category have equal chances
        to be sampled. They are different across different categories though. The way how they change between
        categories control cumulative probability for each category, i.e. a chance for any image from a
        category to be sampled.

        When weighting is not used, sampling uses equal probability for each instance regardless of its category.
        At a category level, this skews chances in favour of most represented categories. The weighting that is
        calculated below is designed to compensate this skew. The rate of compensation controls how strong
        the compensations is and it vary from 0 (no compensation) to 1 (full compensation, i.e. all categories
        have equal chances to be sampled)

        The function should not be used outside of the class
        """
        assert 0.0 <= self.r_ratio <= 1.0
        classes = self.df.groupby('class').size().to_frame('size').reset_index()
        classes['weights'] = (self.r_ratio * sum(classes['size']) / len(classes) +
                              (1 - self.r_ratio) * classes['size']) / classes['size']
        if classes['class'].isin(['']).any():
            classes.loc[classes['class'] == '', 'weights'] = \
                self.n_ratio * sum(classes['size']) / len(classes) + (1 - self.n_ratio)
        self.df = self.df.merge(classes[['weights', 'class']], on='class', how='left')

    def get_indices(self):
        if self.plan is None:
            self.epoch_init()
        return list(self.plan.index)

    def epoch_init(self):
        """This function will make plan data frame that will contain a sampling plan for the whole dataset"""
#        #self.df.sort_values('class', inplace=True)
#        #self.df.reset_index(inplace=True, drop=True)
        epoch_seed = self.sample_ref().sort_values('class').reset_index(drop=True)
        self.plan = pd.DataFrame({'X1': epoch_seed['id'],
                                  'X2': self.make_positives(epoch_seed),
                                  'X3': self.make_negatives(epoch_seed)})
        self.plan['Y1'], self.plan['Y2'], self.plan['Y3'] = self.make_y(epoch_seed)
        if self.ppn_shuffle:
            self.permute_columns()
        self.plan = self.plan.sample(frac=1).reset_index(drop=True)
        return 0

    def sample_ref(self):
        """This function samples an epoch seed - a list of ids that will be used as reference
        for positive and negative pair creation. The sample (and the epoch) might be smaller
        than the full data set for two reasons: epoch_size parameter might be smaller than 1
        and there might be images available only for negative pair selection"""
        return self.df.loc[self.df['neg_only'] == False].sample(self.epoch_size,
                                                                weights='weights',
                                                                replace=self.replace_ref)

    def make_positives(self, seed):
        """make positive samples (random image of the same class).
        it samples images of the same class from a full database for each of the images
        in seed dataset """
        #return self.df.sample(frac=1).sort_values('class').reset_index(drop=True).id
        per_class = seed.groupby('class').size()
        classes_selected = seed['class'].unique()
        return self.df.loc[self.df['class'].isin(classes_selected)].\
            groupby('class').apply(lambda x: x.sample(per_class.loc[x.name],
                                                      replace=True))['id'].reset_index(drop=True)

    def make_negatives(self, seed):
        """Make negative samples (random image from any other class). Iterative approach is used below which
        is not guaranteed especially when number of categories is low."""
        neg_df = seed.copy()
        mask = neg_df['id'] == neg_df['id']
        for i in range(100):
            neg_df.loc[mask] = self.df.sample(len(neg_df), weights=self.df['weights'],
                                              replace=self.replace_neg).reset_index(drop=True).loc[mask]
            mask = neg_df['class'] == seed['class']
            if not np.any(mask):
                break
            if i == 99:
                raise RuntimeError("Could not fill negative pairs in batch compiler")
        return neg_df['id'].reset_index(drop=True)


    def make_y(self, seed):
        """make Ys for reference, positive and negative examples"""
        samples = len(self.plan)
        if self.y_type == 'y':
            return seed['y'], seed['y'], seed['y']
        else:
            return [1]*samples, [1]*samples, [0]*samples

    def permute_columns(self):
        """This function will permute each row randomly so that image IDs and Ys will be permuted synchronously.
        This function exploits the fact that there are only 6 possible permutations of 3 items (X1,X2,X3)
        and instead of applying one of those 6 randomly for each row, it applies one of them it to randomly
        sampled subset of rows"""
        perm_base = [[0, 1, 2],
                     [1, 0, 2],
                     [0, 2, 1],
                     [2, 1, 0],
                     [1, 2, 0],
                     [2, 0, 1]]
        perm_ids = np.random.randint(0, 5, len(self.plan))
        plan_id = self.plan.as_matrix(columns=['X1', 'X2', 'X3'])
        plan_ys = self.plan.as_matrix(columns=['Y1', 'Y2', 'Y3'])
        for i, p in enumerate(perm_base):
            p = np.argsort(p)
            mask = perm_ids == i
            plan_id[mask, :] = plan_id[mask, :][:, p]
            plan_ys[mask, :] = plan_ys[mask, :][:, p]
        self.plan = pd.concat([pd.DataFrame(plan_id, columns=['X1', 'X2', 'X3']),
                               pd.DataFrame(plan_ys, columns=['Y1', 'Y2', 'Y3'])], axis=1)

    def reshape(self, batch_data):
        x, y = self.unzip(batch_data)
        return np.stack(x), np.stack(y)

    @staticmethod
    def unzip(batch_data):
        return zip(*batch_data)

    def load_images(self, ids, loader):
        """This function returns loads images based on the list of filenames provided in ids array"""
        # first prepare the list of image paths
        #img_data, y = zip(*[loader.load_item(file) for file in ids])
        xy = [delayed(loader.load_item)(file) for file in ids]
        xy = delayed(self.reshape, nout=2)(xy)
        return xy

    def make_batch(self, idx, loader):
        """Core function that will create X and Y from the list of indexes provided by the caller.
        X and Y are picked from self.plan dataframe where ids of the whole dataset are pre-arranged.
        X are kept as image ids which are used to load images from disk"""
        if self.plan is None:
            self.epoch_init()
        xy_list = [self.load_images(self.plan.iloc[idx, c], loader) for c in range(0, 3)]
#        xy = xy_list[0].compute()
        xy_delayed = delayed(self.unzip)(xy_list)
        xy = xy_delayed.compute()
        x, y = xy
        x = list(x)
        y = [self.plan.iloc[idx, c].as_matrix() for c in range(3, 6)]
        y = self.transform_y(idx, y)
        return x, y

    def transform_y(self, idx, y):
        if self.y_type in ['id', 'classes']:
            y = self.plan.iloc[idx, :3]
            if self.y_type == 'classes':
                #yy = y['X1'].to_frame('id').merge(self.df[['id', 'class']], on='id', how='left')['class']
                y = y.apply(lambda col: col.to_frame('id').merge(self.df[['id', 'class']],
                                                                 on='id', how='left')['class'], axis=0)
                return y.as_matrix()
            else:
                return y.as_matrix()
        elif self.y_type == '2dist':
            y = np.abs(np.stack([y[1]-y[0], y[2]-y[1]], axis=1))
            return y
        elif self.y_type == '3dist':
            y = np.abs(np.stack([y[1]-y[0], y[2]-y[1], y[2]-y[0]], axis=1))
            return y
        elif self.y_type == 'y':
            y = np.stack(y[0])
            return y
        elif self.y_type == '0':
            y = np.zeros(len(y[0]))
            return y


class CentroidBatch(object):
    def __init__(self, ids, classes, neg_only=None,
                 shuffle=True, epoch_size = 0,
                 r_ratio = 0.0, n_ratio=0.1, nneg = 0):
        if neg_only is None:
            neg_only = [False]*len(ids)
        assert len(neg_only) == len(ids)
        if classes is None:
            exit('Error. Please provide classes')
        else:
            self.df = pd.DataFrame({'id': ids, 'class': classes, 'neg_only': neg_only})
            self.classes = pd.Series(self.df['class'].unique())
        self.df.reset_index(drop=True, inplace=True)
        self.df.loc[self.df['neg_only'], 'class'] = -1
        self.shuffle = shuffle
        self.r_ratio = r_ratio
        self.n_ratio = n_ratio
        self.get_weighs()
        self.nneg = nneg
        self.epoch_size = epoch_size
        if self.epoch_size == 0:
            self.epoch_size = len(self.df)


    def epoch_init(self):
        if self.shuffle:
            self.df = self.df.sample(frac=1).reset_index(drop=True)
        return

    def get_indices(self):
        """This function samples an epoch - a list of ids that will be used
        The sample (and the epoch) might be smaller
        than the full data set for two reasons: epoch_size parameter might be smaller than 1
        or there might be images available only for negative pair selection"""
        if self.shuffle:
            return self.df.sample(self.epoch_size, weights='weights', replace=True).index.tolist()
        else:
            return self.df.head(self.epoch_size).index.tolist()

    @staticmethod
    def reshape(batch_data):
        x, y = zip(*batch_data)
        return np.stack(x), np.stack(y)

    def make_batch(self, ids, loader):
        # X, y = zip(*[loader.load_item(self.df['id'].iloc[i]) for i in ids])
        xy = [delayed(loader.load_item)(self.df['id'].iloc[i]) for i in ids]
        xy = delayed(self.reshape)(xy)
        x, y = xy.compute()
        y = self.make_y(ids, y)
        return [x] + y, [0]*len(x)

    def one_y(self, cl):
        return self.classes[self.classes != cl].sample(self.nneg)

    def make_y(self, ids, y):
        ys = self.df.iloc[ids].copy()
        ys.loc[ys['neg_only'], 'class'] = 0
        y0 = ys['class'].tolist()
        yneg = ys['neg_only'].astype(int).tolist()
        y = [yneg] + [y0]
        if self.nneg > 0:
            yn = np.stack([self.one_y(cl).as_matrix() for cl in y0])
            y = y + np.transpose(yn).tolist()
        y = [np.array(a) for a in y]
        return y

    def get_weighs(self):
        """This function adds new weight column to self.df data frame to use later in sampling. The weights
        do not sum up to 1 but probabilities can easily be calculated by normalizing them.

        The weights are always the same within a category, so instances of same category have equal chances
        to be sampled. They are different across different categories though. The way how they change between
        categories control cumulative probability for each category, i.e. a chance for any image from a
        category to be sampled.

        When weighting is not used, sampling uses equal probability for each instance regardless of its category.
        At a category level, this skews chances in favour of most represented categories. The weighting that is
        calculated below is designed to compensate this skew. The rate of compensation controls how strong
        the compensations is and it vary from 0 (no compensation) to 1 (full compensation, i.e. all categories
        have equal chances to be sampled)

        The function should not be used outside of the class
        """
        assert 0.0 <= self.r_ratio <= 1.0
        classes = self.df.groupby('class').size().to_frame('size').reset_index()
        classes['weights'] = (self.r_ratio * sum(classes['size']) / len(classes) +
                              (1 - self.r_ratio) * classes['size']) / classes['size']
        if classes['class'].isin([-1]).any():
            classes.loc[classes['class'] == -1, 'weights'] = \
                self.n_ratio * sum(classes['size']) / len(classes) + (1 - self.n_ratio)
        self.df = self.df.merge(classes[['weights', 'class']], on='class', how='left')
