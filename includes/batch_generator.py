import numpy as np


class CustomBatchGenerator(object):
    """This is an interface class that pulls together match manager and loader,
    brakes sequence in chunks, dispatches calls to them and yelds results"""

    def __init__(self, batch_maker, loader, batch_size=32, seed=None):
        self.batch_size = batch_size
        self.seed = seed
        self.batch_maker = batch_maker
        self.loader = loader

    def generator(self):
        """
        This method returns a generator that will return images from folder dir
        defined at class initialization.
        Arguments:
            filenames - list of file names in the dir to choose from
        Returns:
             generator
        """
        # if seed was provided set it before epoch loop to avoid same order across epochs
        if self.seed is not None:
            np.random.seed(self.seed)
        # Infinite loop for multiple loops (epochs)
        while 1:
            indices = self.batch_maker.get_indices()
            self.batch_maker.epoch_init()
            batches = self.cut_sequence(indices)
            for b in batches:
                xy = self.batch_maker.make_batch(b, self.loader)
                yield xy

    def cut_sequence(self, idx):
        return np.split(idx, range(self.batch_size, len(idx), self.batch_size))

    def num_batches(self):
        return len(self.cut_sequence(self.batch_maker.get_indices()))



