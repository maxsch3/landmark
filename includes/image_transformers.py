from PIL import Image
import numpy as np


class BaseTransform(object):
    def __init__(self):
        pass

    @staticmethod
    def pad_img(img, target_size, pad_mode, **kwargs):
        img = np.asarray(img)
        # Pad the numpy array
        to_pad = target_size[0] - img.shape[0], target_size[1] - img.shape[1]
        # There are 3 dimensions in img array here: height, width and channels. We only need to pad H and W
        padding = tuple([(max(p // 2, 0), max(p - p // 2, 0)) for p in to_pad] + [(0, 0)])
        img = np.lib.pad(img, padding, mode=pad_mode, **kwargs)
        return Image.fromarray(img)

    @staticmethod
    def resize_fixed(img, ratio):
        if type(ratio) in [float, int]:
            # assumed scaling keeping aspect ratio
            ratio = ratio, ratio
        w, h = round(img.size[0]*ratio[0]), round(img.size[1]*ratio[1])
        return img.resize((w, h), Image.BILINEAR)

    @staticmethod
    def crop(img, target_size, offset=None):
        if offset is None:
            # assumed center crop
            offset = (img.size[0] - target_size[0]) // 2, (img.size[1] - target_size[1]) // 2
        assert offset[0] >= 0
        assert offset[1] >= 0
        return img.crop((offset[0], offset[1], offset[0]+target_size[0], offset[1]+target_size[1]))

    def resize(self, img, target_size, keep_aspect=True, short_side=True, padding=None):
        scales = target_size[0]/img.size[0], target_size[1]/img.size[1]
        if keep_aspect:
            if short_side:
                # resize and crop
                img = self.resize_fixed(img, max(scales))
                img = self.crop(img, target_size)
            else:
                # resize and pad
                img = self.resize_fixed(img, min(scales))
                img = self.pad_img(img, target_size, padding)
        else:
            # just resize without keeping aspect ratio
            img = self.resize_fixed(img, scales)
        return img

    @staticmethod
    def rotate(img, angle):
        return img.rotate(angle, resample=Image.BILINEAR)


class RandomCrop(BaseTransform):
    def __init__(self, target_size, max_side, upscale=False, pad_mode='reflect'):
        super().__init__()
        self.target_size = target_size
        self.max_side = max_side
        self.upscale = upscale
        self.pad_mode = pad_mode

    def random_crop(self, img):
        excess = img.size[0]-self.target_size[0], img.size[1]-self.target_size[1]
        x = np.random.randint(0, excess[0]) if excess[0] > 0 else 0
        y = np.random.randint(0, excess[1]) if excess[1] > 0 else 0
        return self.crop(img, self.target_size, offset=(x, y))

    def random_resize(self, img, crop=True):
        scales = img.size[0]/self.target_size[0], img.size[1]/self.target_size[1]
        # rescale image down if it's short side larger than short_side parameter
        if crop:
            side = np.argmin(scales)
        else:
            side = np.argmax(scales)
        if img.size[side] > self.target_size[side]:
            random_short = np.random.randint(self.target_size[side], min(self.max_side, img.size[side]))
            ratio = random_short/img.size[side]
            img = self.resize_fixed(img, ratio)
        return img

    def transform(self, img):
        if min(img.size) > min(self.target_size):
            img = self.random_resize(img)
        # if image is smaller than target size add mirroring
        if img.size[0] < self.target_size[0] or img.size[1] < self.target_size[1]:
            img = self.pad_img(img, self.target_size, self.pad_mode)
        if img.size[0] > self.target_size[0] or img.size[1] > self.target_size[1]:
            img = self.random_crop(img)
        return img, 0


class Resizer(BaseTransform):
    def __init__(self, target_size, short_side=False, upscale=False, pad_mode='reflect',
                 pad_kwargs=None):
        super().__init__()
        self.target_size = target_size
        self.upscale = upscale
        self.pad_mode = pad_mode
        self.short_side = short_side
        if pad_kwargs is None:
            pad_kwargs = {}
        self.pad_kwargs = pad_kwargs

    def transform(self, img):
        scales = self.target_size[0]/img.size[0], self.target_size[1]/img.size[1]
        if self.short_side:
            ratio = max(scales)
        else:
            ratio = min(scales)
        if self.upscale or ratio < 1:
            img = self.resize_fixed(img, ratio)
        img = self.pad_img(img, self.target_size, self.pad_mode, **self.pad_kwargs)
        return img, 0


class RandomRotate90(BaseTransform):
    def __init__(self, distribution):
        super().__init__()
        self.angles = [0, 90, 180, 270]
        self.distribution = [d/sum(distribution) for d in distribution]

    def transform(self, img):
        ia = np.random.choice(range(len(self.angles)), 1, p=self.distribution)[0]
        img = self.rotate(img, self.angles[ia])
        return img, ia


class RandomRotate(BaseTransform):
    def __init__(self, angles=(-10, 10), distribution='uniform'):
        super().__init__()
        self.angles = angles
        self.distribution = distribution
        self.mean = np.mean(self.angles)
        self.sigma = self.mean - min(angles)

    def random_angle(self):
        if self.distribution == 'uniform':
            return np.random.uniform(self.angles[0], self.angles[1], 1)
        if self.distribution == 'normal':
            return np.random.normal(self.mean, self.sigma, 1)

    def transform(self, img):
        angle = self.random_angle()
        img = self.rotate(img, angle)
        return img, angle