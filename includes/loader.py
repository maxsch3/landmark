import numpy as np
import os
import pickle
import pandas as pd
from PIL import Image
from keras.preprocessing.image import img_to_array
from keras.applications.imagenet_utils import preprocess_input
import bcolz
from multiprocessing import Pool


class ImageLoader(object):
    def __init__(self, source_dir, target_size, target_color='RGB', pad_mode='reflect', keep_aspect=True,
                 imagenet_prep=True, convert_color=False, transformers=None):
        self.source_dir = source_dir
        self.target_size = target_size
        self.pad_mode = pad_mode
        self.keep_aspect = keep_aspect
        self.target_color = target_color
        self.imagenet_prep = imagenet_prep
        self.convert_color = convert_color
        self.transformers = transformers

    def load_item(self, file_name):
        """This function loads image provided by filename, resize it to target size keeping aspect ratio
        and padding to make target size. This function supports padding methods provided by numpy.lib.pad
        function. Please check numpy documentation"""
        img = Image.open(os.path.join(self.source_dir, file_name))
        # Convert image to target color space
        if self.convert_color and img.mode != self.target_color:
            img = img.convert(self.target_color)
        # ratio = min(self.target_size[1]/img.size[0], self.target_size[0]/img.size[1])
        # if ratio < 1.:
        #     new_size = round(img.size[0] * ratio), round(img.size[1] * ratio)
        #     img = img.resize(new_size, resample=Image.BICUBIC)
        # img = img_to_array(img)
        # # Pad the numpy array
        # to_pad = self.target_size[0] - img.shape[0], self.target_size[1] - img.shape[1]
        # # There are 3 dimensions in img array here: height, width and channels. We only need to pad H and W
        # padding = tuple([(p // 2, p - p // 2) for p in to_pad] + [(0, 0)])
        # img = np.lib.pad(img, padding, mode=self.pad_mode)
        y = 0
        if self.transformers is not None:
            for tr in self.transformers:
                img, y = tr.transform(img)
        img = img_to_array(img)
        if self.imagenet_prep:
            return preprocess_input(img), y
        else:
            return img, y


class PickleLoader(object):
    """This object plugs into batch generator object and provides a specific file reading function to it.
    It reads pickle file by its name (with or without extension) provided by calling function as an argument.
    it then decodes into a numpy array and returns it"""
    def __init__(self, source_dir, file_ext=None):
        self.source_dir = source_dir
        self.file_ext = file_ext

    def load_item(self, item):
        """This is the main function of the class that provides its functionality: reading pickle files,
        decoding and returning decoded data as numpy array"""
        file_name = str(item) + '.' + self.file_ext if self.file_ext is not None else str(item)
        with open(os.path.join(self.source_dir, file_name), 'rb') as f:
            x = pickle.load(f)
        return x, 0


class NpyLoader(object):
    """This object plugs into batch generator object and provides a specific file reading function to it.
    It reads numpy.save file by its name (with or without extension) provided by calling function as an argument.
    it then decodes into a numpy array and returns it"""
    def __init__(self, source_dir, file_ext=None):
        self.source_dir = source_dir
        self.file_ext = file_ext

    def load_item(self, item):
        """This is the main function of the class that provides its functionality: reading numpy.save files,
        decoding and returning decoded data as numpy array"""
        file_name = str(item) + '.' + self.file_ext if self.file_ext is not None else str(item)
        return np.load(os.path.join(self.source_dir, file_name)), 0


class NpyCachedLoader(object):
    """This object plugs into batch generator object and provides a specific file reading function to it.
    It reads numpy.save file by its name (with or without extension) provided by calling function as an argument.
    it then decodes into a numpy array and returns it"""
    def __init__(self, source_dir, file_ext=None):
        self.source_dir = source_dir
        self.file_ext = file_ext
        self.cache = self.cache_files()

    def load_item(self, items):
        """This is the main function of the class that provides its functionality: reading numpy.save files,
        decoding and returning decoded data as numpy array"""
        return self.cache[items], 0

    def cache_files(self):
        ids = self.list_images()
        return pd.Series(map(self.load_npy, ids), index=ids)

    def list_images(self):
        files = pd.Series([os.path.splitext(f)[0] for f in os.listdir(self.source_dir) if
                           os.path.isfile(os.path.join(self.source_dir, f))])
        return files

    def load_npy(self, file_id):
        return np.load(os.path.join(self.source_dir, file_id + '.' + self.file_ext))


class PickleCachedLoader(object):
    """This object plugs into batch generator object and provides a specific file reading function to it.
    It reads pickle file by its name (with or without extension) provided by calling function as an argument.
    it then decodes into a numpy array and returns it"""
    def __init__(self, source_dir, file_ext=None):
        self.source_dir = source_dir
        self.file_ext = file_ext
        self.cache = self.cache_files()

    def load_item(self, items):
        """This is the main function of the class that provides its functionality: reading pickle files,
        decoding and returning decoded data as numpy array"""
        return self.cache[items], 0

    def cache_files(self):
        ids = self.list_images()
        return pd.Series(map(self.load_pickle, ids), index=ids)

    def list_images(self):
        files = pd.Series([os.path.splitext(f)[0] for f in os.listdir(self.source_dir) if
                           os.path.isfile(os.path.join(self.source_dir, f))])
        return files

    def load_pickle(self, file_id):
        with open(os.path.join(self.source_dir, file_id + '.' + self.file_ext), 'rb') as f:
            x = pickle.load(f)
        x.astype(float)
        return x


class BcolzLoader(object):
    """This object plugs into batch generator object and provides a specific file reading function to it.
    It opens bcolz array from disk and fetches relevant numpy arrays using index array, which provides mapping
    from file_id to index in bcolz array

    Parameters:
        source_dir - root directory of bcolz array
        index_file - path to an index file where file ids are listed in the order of appearance in bcolz
                     array. This file will be used for mapping of file names (ids) to position in array
        workers -    number of parallel processes to use. This is not working well at all and not recommended
                     to use
        chunksize -  only effective when workers > 1. The size to which list of ids will be cut and split
                     between the pool of workers. Not recommeneded to use
    """
    def __init__(self, source_dir, index_file, workers=1, chunksize=50):
        self.source_dir = source_dir
        self.bc = bcolz.open(rootdir=source_dir)
        self.index = pd.read_csv(index_file, names=['id'])['id']
        self.index = pd.Series(range(len(self.index)), index=self.index)
        assert len(self.bc) == len(self.index)
        self.workers = workers
        self.chunk_size = chunksize
        bcolz.set_nthreads(4)

    def load_bc(self, ids):
        return self.bc[ids]

    def load_item(self, items):
        """This is the main function of the class that provides its functionality: reading numpy.save files,
        decoding and returning decoded data as numpy array"""
        idxs = self.index.loc[items]
        if self.workers > 1:
            with Pool(processes=self.workers) as pool:
                data = np.stack(pool.map(self.load_bc, idxs, chunksize=self.chunk_size))
        else:
            data = self.load_bc(idxs)
        return data, 0
