import torch
import numpy as np


class MeanShift(torch.nn.Module):
    """This class is an implementation of MeanShift algorithm for unsupervised clustering
    of vectors in multidimensional space using cosine similarity as distance metric.
    This is a pytorch implementation that can be run on a GPU"""
    def __init__(self, sigma, batch_size=100, iterations=3, stop_threshold=0.9999):
        """Mean shift class constructor
        Arguments:
            sigma: standard deviation of a gaussian PDF used to weigh distances. The higher the value the less
                clusters will be discovered. This value should be found experimentally, and 0.2 is a good starting
                value
            batch_size: integer number of vectors that are processed in one batch. The higher this value the better
                the performance, but it is limited by the size of memory on your GPU. If the execution fails
                 with out of memory error, decrease this parameter
            iterations: meanshift is an iterative algorithm that pulls vectors towards their centroids at
                each iteration. The more iterations - the closer the vectors will be pushed to their centroids.
                The fact that after few iterations vectors of the same cluster become virtually equal, used
                in centroid determination later. The algorithm just picks the first vector from new cluster
                as a centroid
            treshold: min probability of a vector belonging to a cluster to assign the vector to that cluster
            """
        super(MeanShift, self).__init__()
        self.batch_size = batch_size
        self.iterations = iterations
        self.stop_threshold = stop_threshold
        self.stop_tan = np.tan(np.arccos(stop_threshold))
        self.sigma = sigma
        self.shift = [0.0]*self.iterations
        self.shifted = [0]*self.iterations

    def meanshift_iteration(self, x, original_x):
        batches = list(range(0, len(x), self.batch_size))
        if batches[-1] < len(x):
            batches += [len(x)]
        batches = list(zip(batches[:-1], batches[1:]))
        shifts = torch.zeros(x.shape[0])
        if x.is_cuda:
            shifts = shifts.cuda()
        for start, stop in batches:
            dist = self.cosine_similarity(x[start:stop], original_x)
            weight = self.gravity(dist, 1, self.sigma)
            new_x = torch.matmul(weight, original_x) / torch.sum(weight, dim=1, keepdim=True)
            shifts[start:stop] = torch.sum((x[start:stop] - new_x)**2, dim=1)/torch.sum(new_x**2, dim=1)
            #shifts[start:stop] = x[start:stop] - new_x
            x[start:stop] = new_x
            #calcuating shift to measure how much of a movement was on last iteration
            #shifts[start:stop] = torch.sum(shifts[start:stop]**2, dim=1)/torch.sum(new_x**2, dim=1)
        return x, shifts


    def meanshift(self, x, original_x=None):
        if original_x is None:
            # put aside original X and update a copy of it
            original_x = x
            x = original_x.clone()
        self.shift = [0.0]*self.iterations
        mask = torch.ByteTensor([1]*x.shape[0])
        if x.is_cuda:
            mask = mask.cuda()
        for it in range(self.iterations):
            indexer = mask.nonzero().squeeze()
            x[indexer], shifts =\
                self.meanshift_iteration(x[indexer], original_x)
            # shifts above are effectively tangents of shift angles between old and new vector
            self.shift[it] += torch.sum(shifts)
            self.shifted[it] += sum(shifts > self.stop_tan)
            mask[indexer] = mask[indexer] * (shifts > self.stop_tan)
            print('Iteration %d/%d complete with total shift of %f and %d vectors shifted cos(a) > %g'
                  % (it+1, self.iterations, self.shift[it], self.shifted[it], self.stop_threshold))
            if sum(mask) == 0:
                break
        return x

    def gravity(self, d, mu, sigma):
        return torch.exp(-0.5*((d-mu)/sigma)**2)

    def cosine_similarity(self, x, X):
        return torch.matmul(x, torch.t(X)) / torch.matmul(torch.norm(x, p=2, dim=1, keepdim=True),
                                                         torch.t(torch.norm(X, p=2, dim=1, keepdim=True)))

    def get_centroids(self, X, threshold):
        centroids = X[0].unsqueeze(0)
        clust = torch.zeros(len(X))
        for i, x in enumerate(X):
            dist = self.cosine_similarity(x.unsqueeze(0), centroids)
            #weight = self.gravity(dist, 1.0, self.sigma)
            #prob = weight / torch.sum(weight, dim=1, keepdim=True)
            #max_sim, ci = torch.max(prob, 1)
            max_sim, ci = torch.max(dist, 1)
            if max_sim[0] < threshold:
                clust[i] = len(centroids)
                centroids = torch.cat([centroids, x.unsqueeze(0)], dim=0)
            else:
                clust[i] = ci[0]
        return centroids, clust

    def k_nearest(self, X, centroids, sigma=None, k=1):
        if sigma is None:
            sigma = self.sigma
        if len(X.shape) < 2:
            X = X.unsqueeze(0)
        d = torch.zeros(X.shape[0], k)
        i = torch.zeros_like(d)
        p = torch.zeros_like(d)
        batches = list(range(0, len(X), self.batch_size))
        if batches[-1] < len(X):
            batches += [len(X)]
        batches = list(zip(batches[:-1], batches[1:]))
        for start, stop in batches:
            dist = self.cosine_similarity(X[start:stop], centroids)
            #weight = self.gravity(dist, 1.0, sigma)
            #weight = torch.sum(weight, dim=1, keepdim=True)
            sorted_dist, indices = torch.topk(dist, k, sorted=True)
            prob = self.gravity(sorted_dist, 1.0, sigma)
            d[start:stop] = sorted_dist
            i[start:stop] = indices
            p[start:stop] = prob
        return d, i, p
