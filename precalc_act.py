from keras.applications.resnet50 import ResNet50
from keras.applications.xception import Xception
from keras.applications.vgg16 import VGG16
from keras.applications.vgg19 import VGG19
from keras.applications.inception_v3 import InceptionV3
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.layers import Input, Lambda
from keras.layers import GlobalMaxPooling2D, GlobalAveragePooling2D, AveragePooling2D, MaxPooling2D
from keras.models import Model
import pandas as pd
import os
import pickle
import numpy as np
import argparse
from tqdm import tqdm
import math
import multiprocessing
import bcolz
from includes import batch_generator as bg, loader as ldr, batch_compiler as bc


class BcolzContainer(object):
    def __init__(self, out_dir, shape, params=None):
        self.type = 'bcolz'
        self.out_dir = out_dir
        if os.path.isdir(self.out_dir):
            print('Output directory ' + self.out_dir +
                  ' already exists. Bcolz cannot use existing folder for a new container')
            exit(1)
        self.bc = self.create_bcolz(shape, params)
        self.ids = None

    def create_bcolz(self, shape, params):
        chunklen = 10 if 'bcolz_chunklen' not in params else params.bcolz_chunklen
        dtype = 'float16' if 'target_dtype' not in params else params.target_dtype
        clevel = 5 if 'bcolz_clevel' not in params else params.bcolz_clevel
        return bcolz.carray(np.empty((0,)+shape), rootdir=self.out_dir, chunklen=chunklen,
                            cparams=bcolz.cparams(clevel=clevel), dtype=dtype)

    def append(self, data, ids):
        if self.ids is None:
            self.ids = pd.Series(ids)
        else:
            self.ids = self.ids.append(ids, ignore_index=True)
        self.bc.append(data)
        self.bc.flush()

    def finalize(self):
        print('Finalizing bcolz array ...')
        self.bc.flush()
        print('Writing index file ...')
        self.ids.to_csv(self.out_dir + '_index.csv', index=False)
        print('Data was saved successfully')


class NpyContainer(object):
    def __init__(self, out_dir, shape=None, params=None):
        self.type = 'npy'
        self.out_dir = out_dir
        if self.type == 'npy':
            if not os.path.isdir(self.out_dir):
                print('Output directory ' + self.out_dir +
                      ' does not exist. A valid folder must be provided to save numpy files')
                exit(1)

    def append(self, data, ids):
        for i, item in enumerate(ids):
            np.save(os.path.join(self.out_dir, str(item)+'.npy'), data[i, :])

    def finalize(self):
        return


def create_model(net, args):
    pool_choice = {'gmp': GlobalMaxPooling2D, 'gap': GlobalAveragePooling2D,
                   'mp2d': MaxPooling2D, 'ap2d': AveragePooling2D}
    img_shape = (args.inp_dim[0], args.inp_dim[1], 3)
    # The triplet network takes 3 input images: 2 of the same class and 1 out-of-class sample
    inputs = Input(shape=img_shape)
    # load a pretrained model (try, except block because the kernel would not let me download
    # the weights for the network)
    if 'weights' in args:
        weights = args.weights
    else:
        weights = 'imagenet'
    print('Loading ' + weights + ' weights...')
    try:
        base_model = net(input_shape=img_shape, weights=weights, include_top=False)
    except:
        print('Could not download weights. Using random initialization...')
        base_model = net(input_shape=img_shape, weights=None, include_top=False)
    # make base model non trainable
    for layer in base_model.layers:
        layer.trainable = False
    # find the layer to pick activations from
    if args.from_layer > 0:
        mid_out = base_model.layers[args.from_layer]
        print('Using outputs of layer ', mid_out.name)
        base_model = Model(base_model.inputs, mid_out.output)
    x = base_model(inputs)
    print('Dimensionality of output from the base model: ' + str(x.shape))
    if args.pool_mode in ['mp2d', 'ap2d']:
        print('Applying ' + pool_choice[args.pool_mode].__name__ + ' with pool_size ' +
              str(tuple(args.pool_size)) + ' to the output')
        x = pool_choice[args.pool_mode](pool_size=tuple(args.pool_size))(x)
    else:
        x = pool_choice[args.pool_mode]()(x)
        print('Applying ' + pool_choice[args.pool_mode].__name__ + ' to the output')
    print('Resulting dimensionality of output: ' + str(x.shape))
    model = Model(inputs=inputs, outputs=x)
    model.compile(optimizer='adam', loss='mse')
    return model


def process_arguments():
    parser = argparse.ArgumentParser()
    #parser.add_argument("--file_list", required=True, help="CSV file with the list of all images to process")
    parser.add_argument("--inp_dir", required=True, help="input directory")
    parser.add_argument("--out_dir", required=True, help="input directory")
    parser.add_argument("--inp_dim", nargs=2, required=True, help="height width of an input image"
                                                                  " (the one that is fed into the pre-trained "
                                                                  "system. It can be different from actual image size",
                        type=int)
    parser.add_argument("--model", required=True, help="Pre-trained model from the list of applications"
                                                       " available in keras",
                        choices=['xception', 'vgg16', 'vgg19', 'inceptionV3', 'resnet50', 'IncResNetV2'], type=str)
    parser.add_argument("--batch_size", help="Batch size", type=int, default=32)
    parser.add_argument("--workers", help="Number of parallel processes to use for image extraction and pre-processing",
                        type=int, default=4)
    parser.add_argument("--from_layer", help="Pick activations from layer. Integer index", type=int, default=0)
    parser.add_argument("--pool_mode", help="Choice of how layers are pooled from the model",
                        choices=['gmp', 'gap', 'mp2d', 'ap2d'], default='gmp')
    parser.add_argument("--pool_size", help="Pool size parameter for Average Pooling and MaxPooling output layers",
                        nargs=2, type=int, default=[2, 2])
    parser.add_argument("--target_dtype", help="Data type to be saved", default='float16')
    parser.add_argument("--target_type", help="Save output to different containers", choices=['npy', 'bcolz'])
    parser.add_argument('--bcolz_clevel', help='Compression level of bcolz container', type=int)
    parser.add_argument('--bcolz_chunklen', help='Chunk length used by bcolz', type=int)
    args = parser.parse_args()
    return args


def load_ids(inp_dir):
    files = [[os.path.splitext(f)[0], f] for f in os.listdir(inp_dir) if os.path.isfile(os.path.join(inp_dir, f))]
    files = pd.DataFrame(files, columns=['id', 'file'])
    return files


def get_model(args):
    picker = {'xception': Xception, 'vgg16': VGG16, 'vgg19': VGG19, 'inceptionV3': InceptionV3,
              'resnet50': ResNet50, 'IncResNetV2': InceptionResNetV2}
    Net = picker[args.model]
    return create_model(Net, args)


def pickle_batch(ids, array, out_dir):
    for i, item in enumerate(ids):
        with open(os.path.join(out_dir, str(item)+'.pkl'), 'wb') as f:
            pickle.dump(array[i, :], f, pickle.HIGHEST_PROTOCOL)


def npsave_batch(ids, array, out_dir):
    for i, item in enumerate(ids):
        np.save(os.path.join(out_dir, str(item)+'.npy'), array[i, :])


def create_container(args, shape):
    assert 'target_type' in args
    assert 'out_dir' in args
    if args.target_type == 'npy':
        return NpyContainer(args.out_dir, shape=shape, params=args)
    if args.target_type == 'bcolz':
        return BcolzContainer(args.out_dir, shape=shape, params=args)
    print('Could not create a container class. Exiting ...')
    exit(1)


def run_model(model, ids, cont, args):
    img_dim = args.inp_dim
    batch_size = args.batch_size
    workers = args.workers
    target_dtype = args.target_dtype
    loader = ldr.ImageLoader(args.inp_dir, target_size=img_dim)
    n = len(ids)
    gen = (ids[i:min(i+batch_size, n)] for i in range(0, n, batch_size))
    with multiprocessing.Pool(processes=workers) as pool:
        for batch in tqdm(gen, total=math.ceil(n/batch_size)):
            if workers > 1:
                imgs = pool.map(loader.load_item, batch['file'])
            else:
                imgs = [loader.load_item(img) for img in batch['file']]
            imgs = np.stack(imgs)
            act = model.predict(imgs)
            act = act.astype(target_dtype)
            #npsave_batch(batch['id'], act, out_dir)
            cont.append(act, batch['id'])
    cont.finalize()


def pre_calculate():
    args = process_arguments()
    print('Loading file database ...')
    ids = load_ids(args.inp_dir)
    print('Loading ' + args.model + ' model from Keras applications ...')
    model = get_model(args)
    shape = tuple([int(i) for i in model.output.shape[1:]])
    print('Initializing container ...')
    cont = create_container(args, shape)
    print('Initializing model ...')
    run_model(model, ids, cont, args)


if __name__ == '__main__':
    pre_calculate()
