# -*- coding: utf-8 -*-

# !/usr/bin/python

# Note: requires the tqdm package (pip install tqdm)

# Note to Kagglers: This script will not run directly in Kaggle kernels. You
# need to download it and run it on your local machine.

# Resizes images in multiple threads
# All images will be saved in the JPG format with 90% compression quality.

# Thanks to @maxwell: https://www.kaggle.com/maxwell110/python3-version-image-downloader

import sys, os, multiprocessing, csv
from PIL import Image
import tqdm
import pandas as pd
import re
import argparse
from functools import partial


def list_images(inp_dir):
    files = [[re.sub('\.jpg$', '', f), os.path.join(inp_dir, f)] for f in os.listdir(inp_dir) if os.path.isfile(os.path.join(inp_dir, f))]
    files = pd.DataFrame(files, columns=['id', 'file'])
    return files


def resize_image(img_path, args):
    out_dir = args.out_dir
    new_size = int(args.long_size)
    (img, path, rot) = img_path
    filename = os.path.join(out_dir, '{}.jpg'.format(img))

    try:
        pil_image = Image.open(path)
    except:
        print('Warning: Failed to open image {}'.format(img))
        return 1

    if pil_image.mode != 'RGB':
        try:
            pil_image = pil_image.convert('RGB')
        except:
            print('Warning: Failed to convert image {} to RGB'.format(img))
            return 1

    if rot > 0:
        angle = 360 - rot*90
        try:
            pil_image = pil_image.rotate(angle, expand=True)
        except:
            print('Warning: Failed to rotate image %s' % str(img))
            return 1

    ratio = 1/(max(pil_image.size)/new_size)
    if ratio > 1:
        print('Warning: Image {} is smaller than desired size'.format(img))

    w, h = round(pil_image.size[0]*ratio), round(pil_image.size[1]*ratio)

    try:
        pil_image = pil_image.resize((w, h), Image.BILINEAR)
    except:
        print('Error: Could not resize image {}'.format(img))


    try:
        pil_image.save(filename, format='JPEG', quality=90)
    except:
        print('Warning: Failed to save image {}'.format(filename))
        return 1

    return 0


def process_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--inp_dir", required=True, help="input directory")
    parser.add_argument("--out_dir", required=True, help="input directory")
    parser.add_argument("--long_size", required=True, help="Length of long side", type=int)
    parser.add_argument("--workers", required=True, help="Number of parallel threads to use", type=int)
    parser.add_argument("--rot_data", required=False, help="A csv file with rotation data having columns"
                                                           " id and rotation, which takes values:"
                                                           " r=0-3 meaning angle=r*90 counter clockwise")
    args = parser.parse_args()
    return args


def loader():
    # if len(sys.argv) != 5:
    #     print('Syntax: {} <input_dir> <output_dir> <longest side> <num workers>'.format(sys.argv[0]))
    #     sys.exit(0)
    # (inp_dir, out_dir, long_side, nproc) = sys.argv[1:]
    # nproc = int(nproc)

    args = process_arguments()

    if not os.path.exists(args.out_dir):
        os.mkdir(args.out_dir)

    if not os.path.exists(args.inp_dir):
        print('Input directory ' + args.inp_dir + ' does not exist. Exiting')
        exit(1)
    else:
        print('Loading images from' + args.inp_dir + ' directory')

    file_list = list_images(args.inp_dir)
    if args.rot_data is not None:
        rotations = pd.read_csv(args.rot_data)
        file_list = file_list.merge(rotations[['id', 'rotation']], on='id', how='left')
        file_list['rotation'].fillna(0, inplace=True)
        file_list['rotation'] = file_list['rotation'].astype(int)
    else:
        file_list['rotation'] = [0]*len(file_list)

    print('Found ' + str(len(file_list)) + ' images')
    pool = multiprocessing.Pool(processes=args.workers)  # Num of CPUs
    resize_image_p=partial(resize_image, args=args)
    failures = sum(tqdm.tqdm(pool.imap_unordered(resize_image_p,
                                                 file_list.itertuples(index=False, name=None)),
                             total=len(file_list)))
    print('Total number of failures:', failures)
    pool.close()
    pool.terminate()


# arg1 : data_file.csv
# arg2 : output_dir
if __name__ == '__main__':
    loader()