from includes import batch_generator as bg, loader as ldr, batch_compiler as bc, image_transformers as it
import pandas as pd
import os, pickle
import numpy as np
from PIL import Image


def list_images(inp_dir):
    files = [[os.path.splitext(f)[0], f] for f in os.listdir(inp_dir) if os.path.isfile(os.path.join(inp_dir, f))]
    return files


def pickle_random(ids, out_dir):
    for i in ids:
        with open(os.path.join(out_dir, str(i)+'.pkl'), 'wb') as f:
            pickle.dump(np.random.uniform(size=2048), f)


inp_dir = 'reco_data/originals'
files = pd.DataFrame(list_images(inp_dir))
files.columns = ['id', 'file_name']
source_db = pd.read_csv('reco_data/train_sample.csv').loc[:, ['id', 'landmark_id']]
files = files.merge(source_db, on='id', how='left')
#create full path column
files['full_name'] = files['file_name'].apply(lambda x: os.path.join(inp_dir, x))
# add some made up ids to test negative only
files['neg_only'] = [False]*len(files)
n_neg = 10
#neg = pd.DataFrame({'id': ['n'+str(i) for i in range(n_neg)],
#                    'landmark_id': ['']*n_neg,
#                    'filename': ['']*n_neg,
#                    'neg_only': [True]*n_neg})
#files = files.append(neg)
files.loc[files.index > 90, 'neg_only'] = True

#make test pickle files
#print('Making test pickle files')
#pickle_random(files['id'], 'reco_data/xception')


#batch_maker = bc.PPNRandomBatch(files['id'], files.landmark_id,neg_only=files.neg_only,
#                                y_type='id', y=[1]*len(files['id']),
#                                c_ratio=1.0, epoch_size=50)
batch_maker = bc.PPNRandomBatch(files['file_name'], files.landmark_id,
                                r_ratio=1.0)
cropper = it.RandomCrop((224,224), 480)
rotator = it.RandomRotate([.3,.3,.1,.3])
loader = ldr.ImageLoader("", target_size=(224, 224), transformers=[cropper, rotator])
#loader = ldr.PickleLoader('reco_data/xception', 'pkl')
#loader = ldr.NpyLoader('reco_data/xception', 'npy')

#batch_maker = bc.BaseBatchMaker(files['file_name'], files.landmark_id, y_type='transform')
batch_maker = bc.CentroidBatch(files['full_name'], files.landmark_id, neg_only=files.neg_only,
                               r_ratio=1.0, n_ratio=1.0, nneg=0, shuffle=False)
g = bg.CustomBatchGenerator(batch_maker, loader, batch_size=12)
gen = g.generator()

c = 0
nb = g.num_batches()
print('Number of batches: ' + str(nb))
for k in range(nb):
   x, y = next(gen)
   #print(sum(y[:,0]!=y[:,1]))
   #print('Width of batch: ' + str(len(x)))
   #print('Length of batch: ' + str(len(x[0])))
   #print('Dimension of image: ' + str(x[0][0].shape))
   print('Length of batch: ' + str(len(x)))
   print('Dimension of image: ' + str(x[0].shape))

# test image loader
sample_img = files['file_name'].sample(1)
#sample_img = files['file_name'].loc[files['file_name'] == '93ad6a9c975c068f.jpg']
img = Image.open(os.path.join('reco_data/originals', sample_img.values[0]))
img.save('original.jpg')
cropper = it.RandomCrop((224, 224), 320)
resizer = it.Resizer((224, 224), pad_mode='reflect')
#rotator = it.RandomRotate90([.3,.3,.3,.1])
rotator = it.RandomRotate((-10,10))
#img, meta = cropper.transform(img)
#img = resizer.transform(img)
img, meta = rotator.transform(img)
img.save('cropped.jpg')
print(meta)


