import numpy as np
import torch
from includes import meanshift_pytorch as ms


def generate_data(n_clusters, n_samples, n_dim):
    centroids = np.random.uniform(-35, 35, (n_clusters, n_dim))
    slices = [np.random.multivariate_normal(centroids[i], np.diag([10.]*n_dim), n_samples)
              for i in range(n_clusters)]
    return np.concatenate(slices).astype(np.float32)


X = generate_data(10, 100, 50)
np.random.shuffle(X)
means = ms.MeanShift(sigma=0.1, iterations=10)
X = torch.Tensor(X)
X = means.meanshift(X)
centroids, clust = means.get_centroids(X, threshold=0.95)
print(centroids.shape)
#print(clust[:10])
