from keras.models import Model, load_model
import keras.backend as K
from keras.callbacks import TensorBoard, ReduceLROnPlateau, EarlyStopping
import time
#import pandas as pd
#import os
import pickle
import argparse
#from includes import batch_generator as bg, loader as ldr, batch_compiler as bc


def process_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--gen_file", required=True, help="A file in which generator is pre-saved")
    parser.add_argument("--model_file", required=True, help="A file where Keras model is pre-saved")
    parser.add_argument("--model_out_file", required=True, help="A file where resulting model with new "
                                                                "weights to be saved")
    parser.add_argument("--lr", help="Start from this learning rate", type=float)
    parser.add_argument("--epochs", required=True, help="Start from this learning rate", type=int)
    rop_group = parser.add_argument_group(title='ReduceOnPlateu settings')
    rop_group.add_argument("--min_lr", help="Start from this learning rate")
    rop_group.add_argument("--monitor", help="Monitor either train loss or validation loss for learning rate reduction")
    rop_group.add_argument("--factor", help="Multiply learning rate by this factor", type=float)
    rop_group.add_argument("--patience", help="How many epochs to wait monitored value doesn't change", type=int)
    rop_group.add_argument("--verbose", help="How many epochs to wait monitored value doesn't change", type=int)
    rop_group.add_argument("--mode", help="Direction of monitored value", choices=['auto', 'min', 'max'])
    rop_group.add_argument("--epsilon", help="Direction of monitored value", type=float)
    rop_group.add_argument("--cooldown", help="Direction of monitored value", type=int)

    args = parser.parse_args()
    rop = ['min_lr', 'monitor', 'factor', 'patience', 'verbose', 'mode', 'epsilon', 'cooldown']
    rop_args = {k: v for k, v in vars(args).items() if k in rop and v is not None}
    return rop_args, args


def load_generator(args):
    print('Loading pre-saved generator objects from ' + args.gen_file + ' ...')
    with open(args.gen_file, 'rb') as f:
        gen_trn, gen_val = pickle.load(f)
    return gen_trn, gen_val


def load_model_file(args):
    print('Loading pre-saved model from ' + args.model_file + ' ...')
    return load_model(args.model_file)


def pre_calculate():
    rop_args, args = process_arguments()
    train_gen, val_gen = load_generator(args)
    model = load_model_file(args)
    #train_gen, val_gen, model = None, None, None
    model = train_model(model, train_gen, val_gen, args, rop_args)
    save_model_file(model, args)


def train_model(model, train_gen, val_gen, args, rop_args):
    print('Running the model with the following parameters: ')
    print('  Number of epochs: ' + str(args.epochs))
    if 'lr' in args:
        K.set_value(model.optimizer.lr, args.lr)
        lr = args.lr
    else:
        lr = K.get_value(model.optimizer.lr)
    if len(rop_args) > 0:
        print('  With initial learning rate: ' + str(lr))
        print('  Using ReduceLROnPlateau callback with settings: ')
        for k, v in rop_args.items():
            print('    ' + str(k) + ': ' + str(v))
    else:
        print('  Learning rate: ' + str(args.lr))
    callbacks = []
    start = time.time()
    if len(rop_args) > 0:
        callbacks.append(ReduceLROnPlateau(**rop_args))
    model.fit_generator(train_gen.generator(), steps_per_epoch=train_gen.num_batches(), epochs=args.epochs,
                        validation_data=val_gen.generator(), validation_steps=val_gen.num_batches(),
                        callbacks=callbacks)
    print('Training is finished after ' + str(time.time() - start))
    return model


def save_model_file(model, args):
    print('Saving new trained model to ' + args.model_out_file + ' ...')
    model.save(args.model_out_file)
    print('Model saved successfully')


if __name__ == '__main__':
    pre_calculate()
